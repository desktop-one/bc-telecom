(function ($) {
    "use strict";
    var wWidth = $(window).width();

    jQuery(document).ready(function ($) {

        new VenoBox({
            selector: '.gallery-video',
            maxWidth: '980px',
            autoplay: true,
        });

        //--------Sticky menu---------
        $(window).scroll(function () {
            if($(this).scrollTop() > 20) {
                $('.hazor-header').addClass('sticky');
            } else {
                $('.hazor-header').removeClass('sticky');
            }
        });

        //--------client logo carousel-----
        $('.client-logo-wrapper').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: true,
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
               
            ]

        });

        //--------testimonial carousel-----
        $('.testimonial-content-wrapper').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            asNavFor: '.testimonial-author-wrapper'
        });
        
        $('.testimonial-author-wrapper').slick({
            centerPadding: 0,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.testimonial-content-wrapper',
            dots: false,
            centerMode: true,
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
               
            ]
        });


        //---------twitter feed--------
        $('.service-hero-slider').slick({
            slidesToShow: 1,
            autoplay: true,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            //nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
            //prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
        });

        $('.form-group input').focusout(function() {
            var text_val = $(this).val();
            if (text_val === "") {
                $(this).removeClass('has-value');
            } else {
                $(this).addClass('has-value');
            }
        });

        //-----------show/hide password---------
        $('#show-password').click( function(){
            if( $(this).is(':checked') ){
                $(this).parent().find('input[name="password"]').attr('type', 'text');
                $(this).parent().find('.fa-eye').hide();
                $(this).parent().find('.fa-eye-slash').show();
            }else{
                $(this).parent().find('input[name="password"]').attr('type', 'password');
                $(this).parent().find('.fa-eye').show();
                $(this).parent().find('.fa-eye-slash').hide();
            }
            
        });


        //---------privacy policy navigation------------
        $(".anchor").on("click", function(event) {
            var target = $($(this).attr("data-target"));

            if (target.length) {
                event.preventDefault();
                $("html, body").animate({
                    scrollTop: target.offset().top - 0 + 'px'
                }, 1000);
            }
        });


        //---------single-msg------------
        $('.single-msg-list').click( function(){
            var data_target = $(this).attr('data-target');

            $(this).siblings('.single-msg-list').removeClass('active');
            $(this).addClass('active');

            $('#'+data_target).siblings('.individual-user-msg').hide();
            $('#'+data_target).show();
            
        });


    }); //----end document ready function-----



}(jQuery));