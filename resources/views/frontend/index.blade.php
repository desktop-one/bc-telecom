@extends('frontend.layouts.app')

@section('title') {{app_name()}} @endsection

@section('content')

<section class="bg-gray-100 mb-20 text-center">
    <h2 style="font-size: 50px">{{ setting('app_name') }}</h2>   
    @auth
        <a href="{{ route('backend.dashboard') }}">
            <h2 style="font-size: 30px">Dashboard</h2>
        </a>
    @endauth

    @guest 
        <a href="{{ route('login') }}">
            <h2 style="font-size: 30px">Login</h2>
        </a>
    @endguest
</section>

@endsection