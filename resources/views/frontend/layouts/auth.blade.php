<!DOCTYPE html>
<html lang="en-US" class="no-js">
    <head>
        <title>@yield('title') | {{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="@yield('meta_description', setting('meta_description'))">
        <meta name="keyword" content="@yield('meta_keyword', setting('meta_keyword'))">

        @include('frontend.includes.meta')

        <!--favicon icon-->
        <link rel="shortcut icon" href="{{asset(setting('site_favicon'))}}">
        <link rel="icon" type="image/ico" href="{{asset(setting('site_favicon'))}}" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- font awesome css -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />

        <!--bootstrap min css-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />

        <!--venobox min css-->
        <link rel="stylesheet" href="{{asset('assets/css/venobox.min.css')}}" />

        <!-- slick css -->
        <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">

        <!-- style css -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />

        <script src="https://unpkg.com/alpinejs" defer></script>

        @stack('styles')

        @livewireStyles
    </head>

    @section('body')
        <body>
    @show
            <!-- =======================================
                ==Start Header section==
            =======================================-->
            <header class="ksports-header trans parent">
                @include('frontend.includes.header')
            </header>
            <!-- =======================================
                ==End Header section==
            =======================================-->            
            
            @yield('content')

            <!-- =======================================
                ==Start Footer section==
            =======================================-->
            @include('frontend.includes.footer')

            <!-- =======================================
                ==End Footer section==
            =======================================-->
     
        <!-- jQuary library -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>

        <!--bootstrap js-->
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>

        <!-- venobox -->
        <script src="{{asset('assets/js/venobox.min.js')}}"></script>

        <!-- waypoint -->
        <script src="{{asset('assets/js/slick.min.js')}}"></script>

        <!-- jQuary activation -->
        <script src="{{asset('assets/js/main.js')}}"></script>

        <script>
            $(document).on('click', '.delete-item', function(e) {
                e.preventDefault() // Don't post the form, unless confirmed
                var id = $(this).data('id');
                if (confirm('Are you sure?')) {
                    $('#delete-item-'+id).submit();
                }
            })
        </script>

        <script>
            minimize($('.minimize'));

            function minimize(minimized_elements){
                minimized_elements.each(function(){    
                    var t = $(this).text();        
                    if(t.length < 100) return;
                    
                    $(this).html(
                        t.slice(0,100)+'<span>... </span><a href="#" class="more">More</a>'+
                        '<span style="display:none;">'+ t.slice(100,t.length)+' <a href="#" class="less">Less</a></span>'
                    );
                    
                }); 
                
                $('a.more', minimized_elements).click(function(event){
                    event.preventDefault();
                    $(this).hide().prev().hide();
                    $(this).next().show();        
                });
                
                $('a.less', minimized_elements).click(function(event){
                    event.preventDefault();
                    $(this).parent().hide().prev().show().prev().show();    
                });
            }
        </script>  

        @livewireScripts

        @stack('scripts')
    </body>
</html>
