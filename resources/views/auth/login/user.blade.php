@extends('frontend.layouts.auth')

@section('content')

<section class="ksports-signup ksports-login">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact-us-content-wrapper">

                    <div class="contact-us-left">
                        @include('shared.errors.all')
                        @include('components.alert')
                        <h2 class="form-title">@gTrans('Login')</h2>
                        {{-- <p>There are many variations of passages of Lorem Ipsum available.</p> --}}

                        {{ Aire::open()->method('post') }}
                            @csrf
                            <div class="form-group">
                                <input id="user-email" name="email" type="email" placeholder="{{gTrans('Email Address')}}">
                            </div>

                            <div class="form-group mb-0">
                                <input id="password" name="password" type="password" placeholder="{{gTrans('Password')}}">

                                <label for="show-password" class="show-password" >
                                    <i class="far fa-eye"></i>
                                    <i class="far fa-eye-slash"></i>
                                </label>
                                <input type="checkbox" name="show-password" id="show-password" />
                            </div>

                            <div class="form-group mt-2">
                                <ul>
                                    <li class="tnc-accept checkbox-style">
                                        <label for="remember">
                                            <input type="checkbox" name="remember" id="remember">
                                            <span class="check-box">@gTrans('Remember me')</span>
                                        </label>
                                    </li>

                                    <li>
                                        <a href="{{ route('password.request') }}">@gTrans('Forgot Password')?</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="ksports-btn w-100 mb-3">
                                    @gTrans('Login')
                                </button>
                                <p class="text-center">@gTrans('Don’t have an account')? <a href="{{ route('register',[app()->getLocale(), 'user']) }}">@gTrans('Sign Up')</a></p>
                            </div>
                            
                        {{ Aire::close() }}
                    </div>

                    <div class="contact-us-right">
                       
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection