<header class="jobii-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-xl navbar-light">
                    <a class="navbar-brand" href="{{ route('frontend.index') }}">
                        <img src="{{asset('frontend/images/logo.png')}}" alt="logo">
                    </a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" >
                        <ul class="navbar-nav ml-auto align-items-center">
                            <li class="nav-item {{ activeMenu('frontend.index') }}">
                                <a class="nav-link" href="{{ route('frontend.index') }}" >Home</a>
                            </li>
                            <li class="nav-item {{ activeMenu('frontend.jobs') }}">
                                <a class="nav-link" href="{{ route('frontend.jobs') }}">Jobs </a>
                            </li>
                            <li class="nav-item {{ activeMenu('frontend.about-us') }}">
                                <a class="nav-link" href="{{ route('frontend.about-us') }}">About</a>
                            </li>
                            <li class="nav-item {{ activeMenu('frontend.faq') }}">
                                <a class="nav-link" href="{{ route('frontend.faq') }}">FAQ</a>
                            </li>
                            <li class="nav-item {{ activeMenu('bloglist') }}">
                                <a class="nav-link" href="{{ route('bloglist') }}">Blog</a>
                            </li>
                            <li class="nav-item {{ activeMenu('frontend.contact') }}">
                                <a class="nav-link" href="{{ route('frontend.contact') }}">Contact Us</a>
                            </li>

                            <li class="nav-item job-post-btn">
                                @guest
                                    <a class="nav-link btn-default" href="{{ route('frontend.register-landing') }}">Login/Registration</a>
                                    
                                @endguest

                                @auth
                                
                                    @if(auth()->user()->hasRole('candidate'))
                                        <a class="nav-link btn-default" href="{{ route('frontend.candidates.dashboard') }}">
                                            Dashboard ({{ auth()->user()->full_name }})
                                        </a>
                                    @endif
                                    
                                    @if(auth()->user()->hasRole('company'))
                                        <a class="nav-link btn-default" href="{{ route('frontend.companys.dashboard') }}">
                                            Dashboard ({{ auth()->user()->full_name }})
                                        </a>
                                    @endif

                                    @if(auth()->user()->hasRole('super admin'))
                                        <a class="nav-link btn-default" href="{{ route('backend.dashboard') }}">
                                            Dashboard ({{ auth()->user()->full_name }})
                                        </a>
                                    @endif
                                    
                                @endauth

                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>