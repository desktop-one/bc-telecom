@extends('frontend.layouts.auth')

@section('content')


<section class="hazor-login forgote-pass">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact-us-content-wrapper">
                    <div class="overlay"></div>
                    <div class="contact-us-left">
                        <p>Thank you for creating account. Your email needs to be verified. A verification link is sent to your email. Please follow the link to verify your email. So that you can start using your account. </p>
                        

                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                @lang('home.A fresh verification link has been sent to your email address.')
                            </div>
                        @endif

                        @lang('If you did not receive the email')
                        <form method="POST" action="{{ route('verification.resend', ['email' => $user->email]) }}">
                            @csrf                            
                            <div class="form-group mb-0">
                                <button type="submit" class="hazor-btn w-100 mb-3">@lang('click here to request another')</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
