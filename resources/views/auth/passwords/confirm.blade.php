@extends('frontend.layouts.auth')

@section('content')

    <section class="hazor-login forgote-pass">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact-us-content-wrapper">
                        <div class="overlay"></div>
                        <div class="contact-us-left">
                            <h2 class="form-title">@gTrans('Confirm Password')</h2>
                            <p>{{ gTrans('Please confirm your password before continuing.') }}</p>
                            
                            <form method="POST" action="{{ route('password.confirm') }}">
                                @csrf                         

                                <div class="form-group">
                                    <input id="user-password" name="password" type="password">
                                    <label for="user-password">@gTrans('Password')</label>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>   
                                <div class="form-group mb-0">
                                    <button type="submit" class="hazor-btn w-100 mb-3">@gTrans('password')</button>
                                    @if (Route::has('password.request'))
                                        <a class="hazor-btn" href="{{ route('password.request') }}">
                                            @gTrans('Forgot Password')
                                        </a>
                                    @endif
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
