@extends('frontend.layouts.auth')

@section('content')

<section class="hazor-login forgote-pass">
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('shared.errors.all')
                @include('components.alert')

                <div class="contact-us-content-wrapper">
                    <div class="overlay"></div>
                    <div class="contact-us-left">
                       <a class="hazor-logo" href="{{ route('frontend.index', app()->getLocale()) }}">
                            <x-smart-image src="{{ asset(setting('site_logo_black')) }}" alt="logo" />
                        </a>
                        
                        <h2 class="form-title">@gTrans('Reset Password')</h2>
                        
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf                         

                            <div class="form-group">
                                <label for="email">{{ gTrans('E-Mail Address') }}</label>
                                <input id="email" type="email" class="pass-field @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>   

                            <div class="form-group">

                                <label for="password">@gTrans('password')</label>
                                <input id="password" type="password" class="pass-field @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">


                                <label for="new-password" class="show-password" >
                                    <i class="far fa-eye"></i>
                                    <i class="far fa-eye-slash"></i>
                                </label>
                                <input type="checkbox" name="show-password" id="new-password" class="show-password-checkbox" />

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="form-group">

                                <label for="password-confirm">@gTrans('Confirm Password')</label>

                                <input id="password-confirm" type="password" class="pass-field" name="password_confirmation" required autocomplete="new-password">

                                <label for="confirm-password" class="show-password" >
                                    <i class="far fa-eye"></i>
                                    <i class="far fa-eye-slash"></i>
                                </label>
                                <input type="checkbox" name="show-password" id="confirm-password" class="show-password-checkbox" />
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="hazor-btn w-100 mb-3">@gTrans('Reset your Password')</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
