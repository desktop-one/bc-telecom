@extends('frontend.layouts.auth')

@section('content')

<section class="hazor-login forgote-pass">
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('shared.errors.all')
                @include('components.alert')

                <div class="contact-us-content-wrapper">
                    <div class="overlay"></div>
                    <div class="contact-us-left">
                       <a class="hazor-logo" href="{{ route('frontend.index', app()->getLocale()) }}">
                            <x-smart-image src="{{ asset(setting('site_logo_black')) }}" alt="logo" />
                        </a>
                        
                        <h2 class="form-title">@gTrans('Forgotten Password')</h2>
                        
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf                         

                            <div class="form-group">
                                <input id="user-email" name="email" type="email" value="{{ old('email') }}">
                                <label for="user-email">@gTrans('Email')</label>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>   
                            <div class="form-group mb-0">
                                <button type="submit" class="hazor-btn w-100 mb-3">@gTrans('submit')</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
