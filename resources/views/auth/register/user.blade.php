@extends('frontend.layouts.auth')

@section('content')

<!-- =======Start section===========-->

<section class="ksports-signup">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <div class="contact-us-content-wrapper">
                    <div class="contact-us-left">
                        @include('shared.errors.all')
                        @include('components.alert')
                        
                        <h2 class="form-title">@gTrans('Sign Up')</h2>

                        {{ Aire::open()->method('post') }}
                            <div class="form-group">
                                <select name="type" id="">
                                    <option value="">@gTrans('Select User Type')</option>
                                    <option value="player"  {{ old('type') == 'player' ?'selected':'' }}>@gTrans('Player')</option>
                                    <option value="coach"   {{ old('type') == 'coach'  ?'selected':'' }}>@gTrans('Coach')</option>
                                    {{-- <option value="club"    {{ old('type') == 'club'   ?'selected':'' }}>@gTrans('Club')</option> --}}
                                    <option value="academy" {{ old('type') == 'academy'?'selected':'' }}>@gTrans('Academy')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" placeholder="{{gTrans('First Name')}} *" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <input name="last_name" type="text" placeholder="{{gTrans('Last Name')}} *" value="{{ old('last_name') }}">
                            </div>

                            <div class="form-group">
                                <input name="email" type="email" placeholder="{{gTrans('Emai Address')}}" value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                <input name="phone" type="text" placeholder="{{gTrans('Phone Number')}} *" value="{{ old('phone') }}">
                            </div>

                            <div class="form-group mb-5">
                                <input id="password" name="password" type="password" placeholder="{{gTrans('Password')}}">

                                <label for="show-password" class="show-password" >
                                    <i class="far fa-eye"></i>
                                    <i class="far fa-eye-slash"></i>
                                </label>
                                <input type="checkbox" name="show-password" id="show-password" />
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="ksports-btn w-100 mb-3">
                                    @gTrans('Signup')
                                </button>
                                <p class="text-center">@gTrans('Already have an account')? <a href="{{ route('login', [app()->getLocale(), 'user']) }}">@gTrans('Log In')</a></p>
                            </div>
                            
                        {{ Aire::close() }}
                    </div>

                    <div class="contact-us-right">
                       
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<!-- ========End section===========-->

@endsection