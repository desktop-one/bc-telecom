<div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Phonebook List</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          @foreach($phonebooks as $each)
          <li class="list-group-item">
              <input type="checkbox" id="name_{{ $each->id }}" value="{{ $each->number }}" wire:model="phonenumbers.{{ $each->number }}">
              <label for="name_{{ $each->id }}">{{ $each->name }} - {{ $each->number }}</label>
          </li>
          @endforeach
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" wire:click.prevent="addlistnumber()">Add to number list</button>
      </div>
    </div>
  </div>
</div>

@push('after-scripts')
    <script>
        Livewire.on('openphonebookmodel', postId => {
            $('#exampleModal').modal('show')
        });
        Livewire.on('addtonumberlist', postId => {
            $('#exampleModal').modal('hide')
        });
        $('.close').on('click', function() {
            $('#exampleModal').modal('hide')            
        })
    </script>

@endpush
</div>