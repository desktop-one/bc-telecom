<div>
    <div class="row">
        @foreach($steps as $value=>$each)
        <div class="col">            
            <div class="card">
                <div class="card-header">{{ label_case($value) }}</div>
                <div class="card-body p-0">
                    <div class="list-group">
                        @foreach($each as $value1=>$each1)
                          <a 
                              href="#" 
                              class="list-group-item list-group-item-action 
                              {{ in_array($value1, $activesteps) !== false?'active':'' }}" 
                              wire:click.prevent="addstep('{{$each1['action']}}', '{{ $each1['step'] }}', '{{ $value1 }}')"
                          >
                            <img src="{{ asset($each1['img']) }}" alt="" width="60px">
                            {{ $each1['name'] }}
                          </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
        @endforeach

        @if($laststep)
            <div class="col">            
                <div class="card">
                    <div class="card-header">Number & Amount</div>
                    <div class="card-body p-0" >
                        <div class="col">
                            <button type="button" 
                                class="btn btn-sm btn-{{ $amount==100?'success':'primary' }} mt-1 text-white" 
                                wire:click="$set('amount', 100)"
                            >100</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $amount==200?'success':'primary' }} mt-1 text-white" 
                                wire:click="$set('amount', 200)"
                            >200</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $amount==300?'success':'primary' }} mt-1 text-white" 
                                wire:click="$set('amount', 300)"
                            >300</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $amount==400?'success':'primary' }} mt-1 text-white" 
                                wire:click="$set('amount', 400)"
                            >400</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $amount==500?'success':'primary' }} mt-1 text-white" 
                                wire:click="$set('amount', 500)"
                            >500</button>
                        </div>
                        <div class="col">
                            <button type="button" 
                                class="btn btn-sm btn-{{ $mb=='100Mb'?'success':'warning' }} mt-1 text-white" 
                                wire:click="$set('mb', '100Mb')"
                            >100Mb</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $mb=='200Mb'?'success':'warning' }} mt-1 text-white" 
                                wire:click="$set('mb', '200Mb')"
                            >200Mb</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $mb=='500Mb'?'success':'warning' }} mt-1 text-white" 
                                wire:click="$set('mb', '500Mb')"
                            >500Mb</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $mb=='1Gb'?'success':'warning' }} mt-1 text-white" 
                                wire:click="$set('mb', '1Gb')"
                            >1Gb</button>
                            <button type="button" 
                                class="btn btn-sm btn-{{ $mb=='2Gb'?'success':'warning' }} mt-1 text-white" 
                                wire:click="$set('mb', '2Gb')"
                            >2Gb</button>
                        </div>
                        <div class="input-group mt-2" >
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="">Amount</span>
                          </div>
                          <input type="text" class="form-control" wire:model="amount">
                        </div>
                        <div class="input-group mt-2" >
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="">MB</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Internet" wire:model="mb">
                        </div>
                        <div class="input-group mt-2" wire:ignore>
                            <input type="text" class="form-control" placeholder="Number" wire:model="number">
                            <button type="button" class="btn btn-sm btn-info text-white" wire:click="addnumber()" title="Add Number">
                                <i class="fa fa-plus"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-warning text-white" wire:click="openphonebook()" title="Add from phonebook">
                                <i class="fas fa-book"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <ul class="list-group mt-2" style="list-style-type: none;">
                    @forelse($numbers as $single)
                        <li class="list-group-item">{{ $single }} 
                            <button type="button" class="btn btn-sm btn-danger text-white" wire:click="deletenumber('{{ $single }}')" style="float: right">
                                <i class="fa fa-trash"></i>
                            </button>
                        </li>
                    @empty
                        <li>No number found.</li>
                    @endforelse
                </ul>
                <button type="button" class="btn btn-success w-100 mt-4 text-white">Submit</button>
            </div> <!-- end col -->
        @endif
    </div>
</div>

@push('after-scripts')
    <script>
        $('div.sidebar.sidebar-dark.sidebar-fixed').addClass('hide');
        $('div.container-lg').removeClass('container-lg');
    </script>

@endpush