<div>
    <!-- He who is contented is rich. - Laozi -->
    <a href="#" class="delete-item {{$class}}" data-id="{{$id}}">Delete</a>
    {{ Aire::open()->route($route, $id)->addClass('d-none')->id('delete-item-'.$id) }}

    {{ Aire::close() }}


</div>
