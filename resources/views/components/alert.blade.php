@php
    $alert = $message = null;
@endphp

@if (request()->session()->has('success'))
    @php
        $alert = 'success';
        $message = request()->session()->pull('success');
    @endphp
@elseif (request()->session()->has('error'))
    @php
        $alert = 'danger';
        $message = request()->session()->pull('error');
    @endphp
@endif

@if($alert && $message)
    <div class="alert alert-{{ $alert }} icons-alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled"></i>
        </button>
        <p>{!! $message !!}</p>
    </div>
@endif
