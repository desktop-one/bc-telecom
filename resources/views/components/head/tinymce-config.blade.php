<div>
    <script src="https://cdn.tiny.cloud/1/sjnsuc1f3yecx9mk4qilxe5afks4antxhz3v7wq7ijhqa1l9/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
       tinymce.init({
         selector: 'textarea#{{ $id }}', // Replace this CSS selector to match the placeholder element for TinyMCE
         plugins: 'powerpaste advcode table lists checklist',
         toolbar: 'undo redo | blocks| bold italic | bullist numlist checklist | code | table'
       });
    </script>
</div>