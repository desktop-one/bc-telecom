<a href="{{route('logout', app()->getLocale() )}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="{{ $class }}">

{!! $icon !!}
{{ gTrans($text) }}
</a>

<form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;"> @csrf </form>