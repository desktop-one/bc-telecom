<div class="preloader bg-soft flex-column justify-content-center align-items-center">
    <div class="loader-element">
        {{-- <span class="loader-animated-dot"></span> --}}
        <img src="{{asset(setting('site_preloader'))}}" height="40" alt="{{app_name()}}">
    </div>
</div>
