<div class="row">
    <div class="col-8">
        <div class="form-group">
            <?php
                $field_name = $name;
                $field_lable = label_case($title);
                $field_placeholder = $field_lable;
                $required = '';
            ?>
            {{ html()->label($field_lable, $field_name) }} {!! fielf_required($required) !!}
            {{ html()->input("file", $field_name)->placeholder($field_placeholder)->class('form-control')->attributes(["$required"]) }}
        </div>
    </div>


    <div class="col-4">
        <div class="float-right">
            @if($value)
                <figure class="figure">
                    <a href="{{ asset($value) }}" data-lightbox="image-set" data-title="Path: {{ asset($value) }}">
                        <img src="{{ asset($value) }}" class="figure-img img-fluid rounded img-thumbnail" alt="">
                    </a>
                </figure>
            @endif
        </div>
    </div>
</div>