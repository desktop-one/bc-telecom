<div>
    <?php
        $field_name = $name;
        $field_lable = label_case($title);
        $field_placeholder = $field_lable;
        $required = '';
        $value = $value;
    ?>
    {{ html()->label($field_lable, $field_name) }} {!! fielf_required($required) !!}
    {{ html()->textarea($field_name)->value($value)->placeholder($field_placeholder)->class('form-control')->attributes(["$required"]) }}
</div>