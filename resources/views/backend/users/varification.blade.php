@extends ('backend.layouts.app')

@section('title') {{ __($module_action) }} {{ __($module_title) }} @endsection

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item route='{{route("backend.$module_name.index")}}' icon='{{ $module_icon }}'>
        {{ __($module_title) }}
    </x-backend-breadcrumb-item>

    <x-backend-breadcrumb-item type="active">{{ __($module_action) }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@endsection

@section('content')
<div class="card">
    <div class="card-body">

        <x-backend.section-header>
            <i class="{{ $module_icon }}"></i> {{ __($module_title) }} <small class="text-muted">{{ __($module_action) }}</small>

            <x-slot name="subtitle">
                @lang(":module_name Management Dashboard", ['module_name'=>Str::title($module_name)])
            </x-slot>
            <x-slot name="toolbar">
                <x-buttons.return-back />
                <a href="{{ route("backend.users.index") }}" class="btn btn-primary" data-toggle="tooltip" title="List"><i class="fas fa-list"></i> List</a>
                <a href="{{ route('backend.users.profile', $user->id) }}" class="btn btn-primary" data-toggle="tooltip" title="Profile"><i class="fas fa-user"></i> Profile</a>
                <x-buttons.edit route='{!!route("backend.$module_name.edit", $$module_name_singular)!!}' title="{{__('Edit')}} {{ ucwords(Str::singular($module_name)) }}" class="ms-1" />
            </x-slot>
        </x-backend.section-header>

        <div class="row mt-4 mb-4">
            <div class="col">
            	<livewire:user.verification :user="$user" />
            </div>
        </div>
        <!--/.row-->
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-end text-muted">

                </small>
            </div>
        </div>
    </div>
</div>

@push('after-styles')
	<style>
		.verification-wrapper {
		  background: #FFFFFF;
		  border-radius: 10px;
		  padding: 20px 30px 30px;
		}
		.verification-wrapper .single-verification {
		  margin-bottom: 16px;
		  max-width: 410px;
		  width: 100%;
		}
		.verification-wrapper .single-verification .label {
		  font-weight: 500;
		  font-size: 16px;
		  line-height: 1.3;
		  color: #808282;
		  display: block;
		  margin-bottom: 8px;
		}
		.verification-wrapper .single-verification > p {
		  font-weight: 600;
		  font-size: 18px;
		  line-height: 1.3;
		  color: #060707;
		  display: flex;
		  align-items: center;
		  justify-content: space-between;
		}
		.verification-wrapper .single-verification .img-group {
		  display: flex;
		  flex-flow: row wrap;
		  margin-top: 10px;
		}
		.verification-wrapper .single-verification .img-group img {
		  border: 1px solid #9B9B9B;
		  border-radius: 5px;
		  padding: 7px;
		  width: calc(100% / 2 - 10px);
		}
		.verification-wrapper .single-verification .img-group img:nth-child(odd) {
		  margin-right: 20px;
		}
		.verification-wrapper .single-verification .hazor-btn {
		  margin-top: 30px;
		  font-family: "Lato";
		  font-size: 16px;
		  text-align: center;
		  border-radius: 5px;
		}
	</style>	

@endpush
@endsection