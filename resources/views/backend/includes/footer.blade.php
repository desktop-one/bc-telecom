<footer class="footer text-sm">
    <div>
        <a href="{!! setting('website_url') !!}" target="_blank">{{app_name()}}</a>.
        @if(setting('show_copyright'))
        @lang('Copyright') &copy; {{ date('Y') }}
        @endif
    </div>
    <div class="ms-auto text-muted">{!! setting('footer_text') !!}</div>
</footer>