<?php

namespace Modules\UserPhonebook\Entities;

use App\Models\BaseModel;
use App\Models\Multimedia;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\UserPhonebookData\Entities\UserPhonebookData;

class UserPhonebook extends BaseModel
{
    use SoftDeletes;

    protected $table = 'user_phonebooks';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function multimedia()
    {
        return $this->morphMany(Multimedia::class, 'multimediable');
    }

    public function getImageAttribute()
    {
        return $this->multimedia()->whereLabel('image')->orderByDesc('id')->first()->content ?? 'img/userphonebook.png';
    }
}
