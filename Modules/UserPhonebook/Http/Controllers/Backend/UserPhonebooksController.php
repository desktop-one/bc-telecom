<?php

namespace Modules\UserPhonebook\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Authorizable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Modules\UserPhonebook\Entities\UserPhonebook;
use App\Http\Controllers\Backend\BackendBaseController;
use Yajra\DataTables\DataTables;

class UserPhonebooksController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Phonebook';

        // module name
        $this->module_name = 'userphonebooks';

        // directory path of the module
        $this->module_path = 'userphonebook::backend';

        // module icon
        $this->module_icon = 'fas fa-userphonebooks';

        // module model name, path
        $this->module_model = "Modules\UserPhonebook\Entities\UserPhonebook";

        // redirect url
        $this->red_url = "admin";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Store';

        $validatedData = $request->validate([
            'name'   => 'required|max:191',
            'number' => 'required|max:11',
        ]);

        $$module_name_singular = $module_model::forceCreate(
            [
                'user_id'   => auth()->id(),
                'name'      => $request->name,
                'number'    => $request->number,
            ]
        );

        flash(icon()." ".Str::singular($module_title)."' Created.")->success()->important();
        logUserAccess($module_title.' '.$module_action. " | Id: ". $$module_name_singular->id);
        return redirect($this->red_url."/$module_name");
    }

    public function index_data()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $page_heading = label_case($module_title);
        $title = $page_heading.' '.label_case($module_action);

        $$module_name = $module_model::select('id', 'name', 'number', 'status', 'created_at');

        $data = $$module_name;

        return Datatables::of($$module_name)
                        ->addColumn('action', function ($data) {
                            $module_name = $this->module_name;

                            return view("$this->module_path.$module_name.action_column", compact('module_name', 'data'));
                        })
                        ->editColumn('name', '<strong>{{$name}}</strong>')
                        ->editColumn('created_at', function ($data) {
                            $module_name = $this->module_name;

                            $diff = Carbon::now()->diffInHours($data->created_at);

                            if ($diff < 25) {
                                return $data->created_at->diffForHumans();
                            } else {
                                return $data->created_at->isoFormat('llll');
                            }
                        })
                        ->rawColumns(['name', 'action'])
                        ->orderColumns(['id'], '-:column $1')
                        ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Update';

        $validatedData = $request->validate([
            'name'   => 'required|max:191',
            'number' => 'required|max:11',
        ]);

        $$module_name_singular = $module_model::findOrFail($id);
        $$module_name_singular->update($request->except('_token'));

        flash(icon()." ".Str::singular($module_title)."' Updated Successfully")->success()->important();
        logUserAccess($module_title.' '.$module_action. " | Id: ". $$module_name_singular->id);
        return redirect()->back();
    }
}
