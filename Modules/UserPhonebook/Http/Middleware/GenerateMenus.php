<?php

namespace Modules\UserPhonebook\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         *
         * Module Menu for Admin Backend
         *
         * *********************************************************************
         */
        \Menu::make('admin_sidebar', function ($menu) {

            // UserPhonebooks
            $menu->add('<i class="fas fa-book nav-icon"></i> Phonebook', [
                'route' => 'backend.userphonebooks.index',
                'class' => "nav-item",
            ])
            ->data([
                'order' => 90,
                'activematches' => ['admin/userphonebooks*'],
                'permission' => ['view_userphonebooks'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);
        })->sortBy('order');

        return $next($request);
    }
}
