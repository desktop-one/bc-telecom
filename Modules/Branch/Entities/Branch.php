<?php

namespace Modules\Branch\Entities;

use App\Models\BaseModel;
use App\Models\Multimedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\BranchData\Entities\BranchData;

class Branch extends BaseModel
{
    use SoftDeletes;

    protected $table = 'branchs';

    public function datalist()
    {
        return $this->hasMany(BranchData::class, 'branch_id');
    }

    public function multimedia()
    {
        return $this->morphMany(Multimedia::class, 'multimediable');
    }

    public function getImageAttribute()
    {
        return $this->multimedia()->whereLabel('image')->orderByDesc('id')->first()->content ?? 'img/branch.png';
    }
}
