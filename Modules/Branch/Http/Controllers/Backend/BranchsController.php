<?php

namespace Modules\Branch\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Authorizable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Modules\Branch\Entities\Branch;
use App\Http\Controllers\Backend\BackendBaseController;
use Yajra\DataTables\DataTables;

class BranchsController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Branchs';

        // module name
        $this->module_name = 'branchs';

        // directory path of the module
        $this->module_path = 'branch::backend';

        // module icon
        $this->module_icon = 'fas fa-branchs';

        // module model name, path
        $this->module_model = "Modules\Branch\Entities\Branch";

        // redirect url
        $this->red_url = "admin";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Store';

        $validatedData = $request->validate([
            'title' => 'required|max:191',
        ]);

        $$module_name_singular = $module_model::forceCreate(
            [
                'title'      => $request->title,
                'type'       => $request->type,
                'display_id' => $request->display_id,
                'city'       => $request->city,
                'address'    => $request->address,
                'zip_code'   => $request->zip_code,
                'location'   => $request->location,
                'group'      => $request->group,
                'enabled'    => $request->enabled,
                'testing'    => $request->testing
            ]
        );


        if ($request->hasFile('image')) {
            $$module_name_singular->multimedia()->updateOrCreate(
                ['label' => 'image'],
                ['content' => $this->doUpload($request->image)],
            );
        }

        flash(icon()." ".Str::singular($module_title)."' Created.")->success()->important();
        logUserAccess($module_title.' '.$module_action. " | Id: ". $$module_name_singular->id);
        return redirect($this->red_url."/$module_name");
    }

    public function index_data()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $page_heading = label_case($module_title);
        $title = $page_heading.' '.label_case($module_action);

        $$module_name = $module_model::select('id', 'title', 'updated_at');

        $data = $$module_name;

        return Datatables::of($$module_name)
                        ->addColumn('action', function ($data) {
                            $module_name = $this->module_name;

                            return view("$this->module_path.$module_name.action_column", compact('module_name', 'data'));
                        })
                        ->editColumn('title', '<strong>{{$title}}</strong>')
                        ->editColumn('updated_at', function ($data) {
                            $module_name = $this->module_name;

                            $diff = Carbon::now()->diffInHours($data->updated_at);

                            if ($diff < 25) {
                                return $data->updated_at->diffForHumans();
                            } else {
                                return $data->updated_at->isoFormat('llll');
                            }
                        })
                        ->rawColumns(['designation','title', 'action'])
                        ->orderColumns(['id'], '-:column $1')
                        ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Update';

        $validatedData = $request->validate([
            'title' => 'required|max:191',
        ]);

        $$module_name_singular = $module_model::findOrFail($id);
        $$module_name_singular->update($request->except('_token'));

        if ($request->hasFile('image')) {
            $$module_name_singular->multimedia()->updateOrCreate(
                ['label' => 'image'],
                ['content' => $this->doUpload($request->image, $$module_name_singular->image)],
            );
        }


        flash(icon()." ".Str::singular($module_title)."' Updated Successfully")->success()->important();
        logUserAccess($module_title.' '.$module_action. " | Id: ". $$module_name_singular->id);
        return redirect()->back();
    }
}
