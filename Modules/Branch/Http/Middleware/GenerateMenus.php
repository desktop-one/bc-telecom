<?php

namespace Modules\Branch\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         *
         * Module Menu for Admin Backend
         *
         * *********************************************************************
         */
        \Menu::make('admin_sidebar', function ($menu) {

            // Branchs
            $menu->add('<i class="fas fa-tag nav-icon"></i> Branchs', [
                'route' => 'backend.branchs.index',
                'class' => "nav-item",
            ])
            ->data([
                'order' => 90,
                'activematches' => ['admin/branchs*'],
                'permission' => ['view_branchs'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);
        })->sortBy('order');

        return $next($request);
    }
}
