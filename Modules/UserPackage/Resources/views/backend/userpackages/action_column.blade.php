<div class="text-end">
    @can('edit_'.$module_name)
    <x-buttons.edit route='{!!route("backend.$module_name.edit", $data)!!}' title="{{__('Edit')}} {{ ucwords(Str::singular($module_name)) }}" small="true" />
    @endcan
    
    <a href="#" data-href="{{route("backend.$module_name.destroy", $data)}}" data-msg="{{ isset($deleteMsg)?$deleteMsg:'Are you sure?' }}" class="btn btn-danger btn-sm boot-delete-item" data-toggle="tooltip" title="{{__('labels.backend.delete')}}"><i class="fas fa-trash-alt"></i></a>
</div>
