<?php

namespace Modules\UserPackage\Entities;

use App\Models\BaseModel;
use App\Models\Multimedia;
use App\Models\Title;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\UserPackageData\Entities\UserPackageData;

class UserPackage extends BaseModel
{
    use SoftDeletes;

    protected $table = 'user_packages';

    public function titles()
    {
        return $this->morphMany(Title::class, 'titleable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function multimedia()
    {
        return $this->morphMany(Multimedia::class, 'multimediable');
    }

    public function getImageAttribute()
    {
        return $this->multimedia()->whereLabel('image')->orderByDesc('id')->first()->content ?? 'img/userpackage.png';
    }
}
