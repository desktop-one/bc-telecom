<?php

namespace Modules\UserPackage\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         *
         * Module Menu for Admin Backend
         *
         * *********************************************************************
         */
        \Menu::make('admin_sidebar', function ($menu) {

            // UserPackages
            $menu->add('<i class="fas fa-paper-plane nav-icon"></i>Packages', [
                'route' => 'backend.userpackages.index',
                'class' => "nav-item",
            ])
            ->data([
                'order' => 90,
                'activematches' => ['admin/userpackages.*'],
                'permission' => ['create_userpackages'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

        })->sortBy('order');

        return $next($request);
    }
}
