<livewire:user-request.create />
<livewire:user-request.phonebook />

@push('after-scripts')
    <!-- Lightbox2 Library -->
    <x-library.lightbox/>

    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

    <script>
        // CKEDITOR.replace( 'description' );

    </script>
@endpush