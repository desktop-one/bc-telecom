<?php

namespace Modules\UserRequest\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Authorizable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Modules\UserRequest\Entities\UserRequest;
use App\Http\Controllers\Backend\BackendBaseController;
use Yajra\DataTables\DataTables;

class UserRequestsController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'User Requests';

        // module name
        $this->module_name = 'userrequests';

        // directory path of the module
        $this->module_path = 'userrequest::backend';

        // module icon
        $this->module_icon = 'fas fa-userrequests';

        // module model name, path
        $this->module_model = "Modules\UserRequest\Entities\UserRequest";

        // redirect url
        $this->red_url = "admin";
    }

    public function request_reseller_index()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $$module_name = $module_model::paginate();

        logUserAccess($module_title.' '.$module_action);

        return view(
            "$module_path.$module_name.reseller_index_datatable",
            compact('module_title', 'module_name', "$module_name", 'module_icon', 'module_name_singular', 'module_action')
        );
    }


    public function request_reseller_index_data()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $page_heading = label_case($module_title);
        $title = $page_heading.' '.label_case($module_action);

        $$module_name = $module_model::select('id', 'type', 'updated_at');

        $data = $$module_name;

        return Datatables::of($$module_name)
                        ->addColumn('action', function ($data) {
                            $module_name = $this->module_name;

                            return view("$this->module_path.$module_name.action_column", compact('module_name', 'data'));
                        })
                        ->editColumn('type', '<strong>{{$type}}</strong>')
                        ->editColumn('created_at', function ($data) {
                            $module_name = $this->module_name;

                            $diff = Carbon::now()->diffInHours($data->updated_at);

                            if ($diff < 25) {
                                return $data->updated_at->diffForHumans();
                            } else {
                                return $data->updated_at->isoFormat('llll');
                            }
                        })
                        ->rawColumns(['type', 'action'])
                        ->orderColumns(['id'], '-:column $1')
                        ->make(true);
    }


    public function index_data()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $page_heading = label_case($module_title);
        $title = $page_heading.' '.label_case($module_action);

        $$module_name = $module_model::select('id', 'type', 'updated_at');

        $data = $$module_name;

        return Datatables::of($$module_name)
                        ->addColumn('action', function ($data) {
                            $module_name = $this->module_name;

                            return view("$this->module_path.$module_name.action_column", compact('module_name', 'data'));
                        })
                        ->editColumn('type', '<strong>{{$type}}</strong>')
                        ->editColumn('created_at', function ($data) {
                            $module_name = $this->module_name;

                            $diff = Carbon::now()->diffInHours($data->updated_at);

                            if ($diff < 25) {
                                return $data->updated_at->diffForHumans();
                            } else {
                                return $data->updated_at->isoFormat('llll');
                            }
                        })
                        ->rawColumns(['type', 'action'])
                        ->orderColumns(['id'], '-:column $1')
                        ->make(true);
    }

}
