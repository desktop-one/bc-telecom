<?php

namespace Modules\UserRequest\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         *
         * Module Menu for Admin Backend
         *
         * *********************************************************************
         */
        \Menu::make('admin_sidebar', function ($menu) {

            // UserRequests
            $menu->add('<i class="fas fa-paper-plane nav-icon"></i>Create Requests', [
                'route' => 'backend.userrequests.create',
                'class' => "nav-item",
            ])
            ->data([
                'order' => 90,
                'activematches' => ['admin/userrequests/create'],
                'permission' => ['create_userrequests'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

            // UserRequests
            $menu->add('<i class="fas fa-list nav-icon"></i>List Requests', [
                'route' => 'backend.userrequests.index',
                'class' => "nav-item",
            ])
            ->data([
                'order' => 91,
                'activematches' => ['admin/userrequests'],
                'permission' => ['view_userrequests'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

            // UserRequests
            $menu->add('<i class="fas fa-users nav-icon"></i>Reseller Requests', [
                'route' => ['backend.userrequests.request_reseller_index'],
                'class' => "nav-item",
            ])
            ->data([
                'order' => 92,
                'activematches' => ['admin/userrequests/request_reseller_index'],
                'permission' => ['view_reseller_userrequests'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

        })->sortBy('order');

        return $next($request);
    }
}
