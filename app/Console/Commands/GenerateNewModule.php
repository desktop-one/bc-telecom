<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateNewModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:module {from} {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generage new admin module';

    public $folder;
    public $searchUp;
    public $searchLow;
    public $replaceUp;
    public $replaceLow;
    public $destination;
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->folder = 'Modules/';
        $this->searchUp = $this->argument('from'); 
        $this->searchLow = strtolower($this->argument('from'));
        $this->replaceUp = $this->argument('to'); 
        $this->replaceLow = strtolower($this->argument('to'));

        
        $this->createModule();
        $sourceFiles = $this->sourceFiles();
        $sourceFolders = $this->sourceFolders();

        
        $this->updateFile1($sourceFiles);
        echo "File Done 1! \n";

        $this->updateFile2($sourceFiles);
        echo "File Done 2! \n";

        foreach ($sourceFolders as $foldername) {
            rename( $foldername['source'], $foldername['replace'] );
        }
        echo "Folder Done! \n";


        echo 'All Done!';
    }

    protected function createModule()
    {
        $source = $this->folder.$this->searchUp;
        $this->destination = 'Modules/'.$this->replaceUp;
        if(\File::exists($this->destination)) {
            echo 'Folder already exist';
            exit;
        }
        \File::copyDirectory(base_path($source), base_path($this->destination));
    }

    protected function updateFile1($sourceFiles)
    {
        foreach ($sourceFiles as $filename) {
            $file = file_get_contents("$filename");

            if(stripos($file, $this->replaceLow) !== false){
                echo "Already Replaced \n";
            } else {
                // replace case upper
                $str = str_replace($this->searchUp, $this->replaceUp, $file);
                file_put_contents("$filename", $str); 

            }
        }
    }

    protected function updateFile2($sourceFiles)
    {
        foreach ($sourceFiles as $filename) {
            $file = file_get_contents("$filename");

            if(strpos($file, $this->replaceLow) !== false){
                echo "Already Replaced \n";
            } else {
                // replace case upper
                $str = str_replace($this->searchLow, $this->replaceLow, $file);
                file_put_contents("$filename", $str); 

            }
        }
    }

    protected function sourceFiles()
    {
        return [
            $this->destination.'/Entities/'.$this->searchUp.'.php',
            $this->destination.'/Providers/'.$this->searchUp.'ServiceProvider.php',
            $this->destination.'/Http/Controllers/Backend/'.$this->searchUp.'sController.php',

            $this->destination.'/module.json',
            $this->destination.'/package.json',
            $this->destination.'/composer.json',
            $this->destination.'/Routes/web.php',
            $this->destination.'/webpack.mix.js',
            $this->destination.'/Config/config.php',
            $this->destination.'/Http/Middleware/GenerateMenus.php',
            $this->destination.'/Providers/RouteServiceProvider.php',
        ];
    }

    protected function sourceFolders()
    {
        return [
            [
                'source' => $this->destination.'/Resources/views/backend/'.$this->searchLow.'s',
                'replace' => $this->destination.'/Resources/views/backend/'.$this->replaceLow.'s',
            ],
            [
                'source' => $this->destination.'/Entities/'.$this->searchUp.'.php',
                'replace' => $this->destination.'/Entities/'.$this->replaceUp.'.php',
            ],
            [
                'source' => $this->destination.'/Providers/'.$this->searchUp.'ServiceProvider.php',
                'replace' => $this->destination.'/Providers/'.$this->replaceUp.'ServiceProvider.php',
            ],
            [
                'source' => $this->destination.'/Http/Controllers/Backend/'.$this->searchUp.'sController.php',
                'replace' => $this->destination.'/Http/Controllers/Backend/'.$this->replaceUp.'sController.php',
            ],
        ];
    }
}
