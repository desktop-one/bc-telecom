<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function _upload(UploadedFile $uploadedFile): string
    {
        $files = [
            'name' => (string)Str::uuid(),
            'extension' => config('custom.mime_map.'.$uploadedFile->getClientMimeType()),
        ];

        Storage::disk('public')->put(collect($files)->implode('.'), file_get_contents($uploadedFile));

        // this is an image
        if (str_starts_with($uploadedFile->getMimeType(), 'image')) {
            foreach (config('image.resolutions') as $resolution) {
                $image = Image::make($uploadedFile)
                    ->resize($resolution['width'], $resolution['height'], function ($constraint) {
                        //$constraint->aspectRatio();
                    })->stream();
                Storage::disk('public')->put((($files['name'] . '_' . $resolution['width'] . 'X' . $resolution['height'] . '.') . $files['extension']), $image);
            }
        }

        return Storage::url(collect($files)->implode('.'));
    }

    public function doUpload(UploadedFile $uploadedFile, $previous=null)
    {
        $file = ((string) Str::uuid()) . '.' . $uploadedFile->getClientOriginalExtension();
        Storage::disk('public')->put($file, file_get_contents($uploadedFile));
        // remove previous file
        if($previous != null) {
            if(Storage::disk('public')->has( str_replace('/storage/', '', $previous) )){
                Storage::disk('public')->delete(str_replace('/storage/', '', $previous));
            }
        }
        $url = Storage::url($file);
        // app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('public'.$url);
        return $url;
    }

    // remove indication count
    public function removeLog($flag, $logableId=null, $senderId=null, $receiverId=null)
    {
        $log = Log::whereLevel($flag);
        if($logableId != null) {
            $log->where('logable_id', $logableId);
        }
        if($senderId != null) {
            $log->where('sender_id', $senderId);
        }
        if($receiverId   != null) {
            $log->where('receiver_id', $receiverId);
        }

        return $log->forceDelete();
    }
}
