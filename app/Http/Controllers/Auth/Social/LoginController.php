<?php

namespace App\Http\Controllers\Auth\Social;

use App\Http\Controllers\Controller;
use App\Models\User;
use Socialite;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($type)
    {
        return Socialite::driver($type)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($type)
    {
        $user = Socialite::driver($type)->user();
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            if($type == 'google') {
                $newUser->first_name      = $user->user['given_name'];
                $newUser->last_name       = $user->user['family_name'];
                $newUser->google_id       = $user->id;
            } elseif($type == 'facebook') {
                $newUser->first_name      = $user->name;
                $newUser->last_name       = $user->nickname;
                $newUser->facebook_id     = $user->id;
            }
            $newUser->email           = $user->email;
            $newUser->save();

            $newUser->assignRole(['user']);
            $newUser->images()->create([
                'level' => 'social-avatar',
                'url' => $user->avatar
            ]);

            $username = config('app.initial_username')+$newUser->id;
            $newUser->username = $username;
            $newUser->save();


            auth()->login($newUser, true);
        }
        return redirect()->to('/dashboard');
    }
}