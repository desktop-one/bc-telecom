<?php

namespace App\Http\Controllers\Auth;

use App\Events\Frontend\UserRegistered;
use App\Http\Controllers\Controller;
use App\Jobs\SendMultiMailWithoutLogin;
use App\Models\CompanyProfile;
use App\Models\Subscription;
use App\Models\TempUser;
use App\Models\User;
use App\Models\UserSubscription;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Modules\SitePackage\Entities\SitePackage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    private $user;

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($locale, $role='user')
    {
        return view('auth.register.'.$role);
    }

    public function register(Request $request, $locale, $role = 'user')
    {
        $rules = $this->rules();
        $this->validate($request, $rules, ['name.required' => 'The first name field is required.']);

        $userTable = $this->data($request->all());
        $user =  User::forceCreate($userTable);
        $user->assignRole($request->type);
        
        \Illuminate\Support\Facades\Artisan::call('optimize:clear');
        
        if (\Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // \Mail::to($user->email)->send(new \App\Mail\WelcomeEmail($user));
            // session()->put('user_id_temp', $user->id);

            // $data['subscriptions'] = SitePackage::latest()->get();
            // return view('frontend.subscription')->with($data);
            $user->sendEmailVerificationNotification();
            \Mail::to($user->email)->send(new \App\Mail\AccountConfirmEmail($user));
            

            \Session::flash('success', 'Successfully account created. Please check your email for verify this account.'); 
            // return redirect()->route('verification.notice', ['email' => $user->email]);
            return redirect()->route('login', [app()->getLocale(), 'user']);
        }
        
    }

    // public function redirectTo()
    // {
    //     return route('frontend.index');
    // }

    // user
    private function data($data)
    {
        $userTable = [
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'is_confirm' => 0,
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];

        return $userTable;
    }

    private function rules()
    {
        $rules = [
            'type' => ['required','in:player,coach,club,academy'],
            'name' => ['required', 'max:255'],
            'last_name' => ['required', 'max:255'],
            'phone' => ['required', 'string',],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string'],
        ];

        return $rules;
    }
}
