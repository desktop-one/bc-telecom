<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;


class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend', 'show');
    }

    public function verify(Request $request)
    {
        $user = User::where('id', $request->route('id'))
            ->whereNull('email_verified_at')
            ->first();

        if(!$user) {
            return redirect()->route('frontend.index', app()->getLocale())->withErrors('Invalid request');
        }

        if (! hash_equals((string) $request->route('hash'), sha1($user->getEmailForVerification()))) {
            return redirect()->route('frontend.index', app()->getLocale())->withErrors('Invalid request');
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        auth()->login($user);

        if($user->hasRole('player')) {
            $role = 'frontend.players';
        } 
        if($user->hasRole('coach')) {
            $role = 'frontend.coachs';
        } 
        if($user->hasRole('club')) {
            $role = 'frontend.clubs';
        } 
        if($user->hasRole('academy')) {
            $role = 'frontend.academys';
        } 

        \Session::flash('success', 'Verification completed!'); 
        return redirect()->route($role.'.dashboard', app()->getLocale());
    }

    public function resend(Request $request)
    {        
        if($request->email) {
            $user = User::where('email', $request->email)
            ->whereNull('email_verified_at')
            ->firstOrFail();
            $user->sendEmailVerificationNotification();

            \Session::flash('success', 'Please check your email for verify this account.'); 
            return back();
        }
    }

    public function show(Request $request)
    {
        if(auth()->check()) {
            $user = User::where('email', auth()->user()->email)
            ->whereNull('email_verified_at')
            ->first();

            if(!$user) {
                return redirect()->route('frontend.index', app()->getLocale())->withErrors('Invalid request');
            }
            return view('auth.verify', compact('user'));
        } else {
            return back()->withErrors('Please login.');
        }
    }
}
