<?php

namespace App\Http\Controllers\Auth;

use App\Events\Frontend\UserRegistered;
use App\Http\Controllers\Controller;
use App\Models\CompanyProfile;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Ixudra\Curl\Facades\Curl;
use Stripe\Checkout\Session;
use Stripe\Price;
use Stripe\Product;
use Stripe\Stripe;
use Modules\Plan\Entities\Plan;
use App\Events\AppIncomeCreated;

class SubscriptionController extends Controller
{

    public function index(Request $request)
    {
        if($request->filled('session_id')){
            Stripe::setApiKey(config('services.stripe.secret'));

            $checkoutSession = Session::retrieve($request->session_id);
            $subscription = Subscription::whereStripeId($request->session_id)->firstOrFail();
            $subscription->stripe_id = $checkoutSession->subscription ?? null;
            $subscription->status = 1;
            $subscription->update();

            event(new AppIncomeCreated($subscription, 'subscription'));
            
            // send to web hook if payment success then user will active subscritpion
            // set subcription active
            // if($request->filled('userId')){
            //     $user = User::findOrFail($request->userId);
            //     $user->subscription_active = 1;
            //     $user->save();
            // }
        }
        if($request->filled('userId')){
            $user = User::findOrFail($request->userId);
            $user->email_verified_at = Carbon::now();
            $user->referral_code = gReferral().$user->id;
            $user->is_confirm = 1;
            $user->save();

            event(new UserRegistered($user));


            auth()->login($user);
        }


        return redirect()->route('frontend.company.dashboard', app()->getLocale())->withSuccess(gTrans('Subscription has been created successfully'));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string','confirmed'],
            'country' => ['required'],
            'state' => ['required'],
            'business_name' => ['required'],
        ], [
            'email.required' => trans('home.The email field is required.'),
            'password.required' => trans('home.The password field is required.'),
            'country.required' => trans('home.The country field is required.'),
            'business_name.required' => trans('home.The business name field is required.'),
        ]);

        // dd($request->all());
        $data['company'] = $request->all();
        $data['packages'] = Plan::where('lang', session('locale'))->get();
        return view('auth.subscription-plan')->with($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $data = DB::transaction(function () use ($request) {
            // register new user, force login
            $user = $this->user($request);
            $subscription = $this->subscription($user, $request);            
            
            return [
                'user' => $user,
                'subscription' => $subscription
            ];
        });

        // if user select free plan
        if($request->price == 0) {
            auth()->login($data['user']);
            return redirect()->route('frontend.company.dashboard', app()->getLocale())->withSuccess(gTrans('Subscription has been created successfully'));
        }

        if(isset($data['user']) && isset($data['subscription'])) {
            // auth()->login($data['user']);
            // return $this->stripe($request, $data['subscription'], $data['user']->id);
        }
    }

    private function stripe(Request $request, $mySubscription, $userId)
    {

        Stripe::setApiKey(config('services.stripe.secret'));

        $product = Product::create([
            'name' => 'Subscription '.$request->billing,
            'description' => 'Description',
        ]);

        $price = Price::create([
            'currency' => strtolower(config('services.currency')),
            'unit_amount' => $request->price * 100,
            'recurring' => [
                'interval' => strtolower($request->billing),
                'interval_count' => $request->recurring_after
            ],
            'product' => $product->id
        ]);
        // See https://stripe.com/docs/api/checkout/sessions/create
        // for additional parameters to pass.
        // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
        // the actual Session ID is returned in the query parameter when your customer
        // is redirected to the success page.
        $checkoutSession = Session::create([
            'success_url' => route('subscriptions.index') . '?session_id={CHECKOUT_SESSION_ID}&userId='.$userId,
            'cancel_url' => route('subscriptions.create', ['billing' => $request->billing]),
            'payment_method_types' => ['card'],
            'mode' => 'subscription',
            'line_items' => [[
                'price' => $price->id,
                // For metered billing, do not pass quantity
                'quantity' => 1,
            ]],
        ]);

        $mySubscription->stripe_id = $checkoutSession['id'];
        $mySubscription->update();

        return view('auth.subscription')->withCheckoutSessionId($mySubscription->stripe_id);
    }

    private function user(Request $request)
    {
        $company = json_decode($request->company);
        $userTable = [
            'first_name' => '',
            'last_name' => '',
            'username' => '',           
            // 'email_verified_at' => Carbon::now(),
            // 'is_confirm' => 1,
            'subscription_active' => 0,
            'name' => $company->business_name,
            'email' => $company->email,
            'mobile' => $company->phone_number,
            'password' => Hash::make($company->password),
        ];

        $user =  User::create($userTable);
        $user->assignRole(['company']);

        $companyTable['business_name'] = $company->business_name;
        $companyTable['contact_person'] = $company->contact_person;
        $companyTable['country'] = $company->country;
        $companyTable['city'] = $company->city;
        $companyTable['state'] = $company->state;
        $companyTable['user_id'] = $user->id;
        CompanyProfile::create($companyTable);

        // if user select free plan
        if($request->price == 0) {
            $user->email_verified_at = Carbon::now();
            $user->referral_code = gReferral().$user->id;
            $user->is_confirm = 1;
            $user->subscription_active = 1;
            $user->save();

            event(new UserRegistered($user));
        }

        return $user;
    }

    private function subscription($user, Request $request)
    {
        $subcription = Subscription::forceCreate([
            'user_id' => $user->id,
            'amount' => $request->price,
            'type' => $request->billing,
            'recurring_after' => $request->recurring_after,
            'package_id' => $request->package_id,
            'is_pendding' => $request->price!=0?1:0, // if free plan then it active, if paid then pending after wehook evend this status will active
        ]);        

        return $subcription;


    }
}
