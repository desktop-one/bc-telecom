<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Carbon\Carbon;
use App\Models\Job;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class FrontendController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        return view('frontend.index')->with($data);
    }

    public function page($lang, $pagename)
    {
        $data = [];
        if(view()->exists('frontend.'.$pagename)){
            $function        = \Str::camel($pagename);
            $data[$function] = $this->$function();
            return view('frontend.'.$pagename)->with($data);
        }
        abort(404);
    }

    private function termsCondition()
    {
        $data = [];
        return $data;
    }

    private function faq()
    {
        $data = [];
        return $data;
    }

    private function privacyPolicy()
    {
        $data = [];
        return $data;
    }


}
