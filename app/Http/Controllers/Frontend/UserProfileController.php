<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $lang, $slug)
    {
        $data   = [];
        $userId = \Str::of($slug)->explode('-')->last();
        $user   = User::findOrFail($userId);
        if($slug !== $user->slug) {
            return back()->withErrors('Invalid user.');
        }
        $data['user'] = $user;
        return view('frontend.profile')->with($data);
    }
}
