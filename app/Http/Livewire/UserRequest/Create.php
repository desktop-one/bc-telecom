<?php

namespace App\Http\Livewire\UserRequest;

use Livewire\Component;

class Create extends Component
{
    public $data = [];
    public $steps = [];
    public $activesteps = [];
    public $laststep = false;
    public $amount = 0;
    public $mb = 0;
    public $number;
    public $numbers = [];
    protected $listeners = ['addtonumberlist'];

    public function render()
    {
        return view('livewire.user-request.create');
    }

    public function mount()
    {
        $this->data = config('telecom_data');
        $this->steps['type'] = $this->data['type'];
    }

    public function addstep($index, $step, $active)
    {
        if($step == 1) {
            $this->reset('steps','activesteps','laststep','numbers','number','amount','mb');
        }
        if($step == 1 || $step == 2) {
            // $this->reset('laststep');
            $this->steps['type'] = $this->data['type'];
        }
        $this->activesteps[$index] = $active;
        if($index == 'last') {
            $this->laststep = true;
        } else {            
            $this->steps[$index] = $this->data[$index];
        }
    }

    public function addnumber()
    {
        $this->numbers[$this->number] = $this->number;
        $this->reset('number');
    }

    public function deletenumber($number)
    {
        unset($this->numbers[$number]);
    }

    public function openphonebook()
    {
        $this->emit('openphonebookmodel', $this->numbers);
    }

    public function addtonumberlist($phonebooknumbers)
    {
        if(!empty($phonebooknumbers)) {
            foreach($phonebooknumbers as $each) {
                $this->numbers[$each] = $each;
            }
        }
    }
}
