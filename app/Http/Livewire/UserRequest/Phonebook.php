<?php

namespace App\Http\Livewire\UserRequest;

use Livewire\Component;
use Modules\UserPhonebook\Entities\UserPhonebook;

class Phonebook extends Component
{
    public $phonenumbers = [];
    public $activenumbers = [];
    public $phonebooks = [];
    protected $listeners = ['openphonebookmodel'];

    public function render()
    {
        return view('livewire.user-request.phonebook');
    }

    public function mount()
    {
        $this->phonebooks = UserPhonebook::where('user_id', auth()->id())->get();        
    }

    public function openphonebookmodel($numbers)
    {
        if(!empty($numbers)) {
            foreach($numbers as $number) {
                $this->phonenumbers[$number] = $number;
            }
        }
    }

    public function addlistnumber()
    {
        $this->emit('addtonumberlist', $this->phonenumbers);
        $this->reset('phonenumbers');
    }
}
