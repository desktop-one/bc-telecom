<?php

namespace App\Http\Livewire\UserPackage;

use Livewire\Component;

class Create extends Component
{
    public $data = [];
    public $steps = [];
    public $activesteps = [];
    public $activedetails = [];
    public $laststep = false;
    public $amount = 0;
    public $mb = 0;

    public function render()
    {
        return view('livewire.user-package.create');
    }

    public function mount()
    {
        $this->data = config('telecom_data');
        $this->steps['type'] = $this->data['type'];
    }

    public function addstep($index, $step, $active, $datamain)
    {
        // dump(json_decode($datamain, true), $active);
        $maindata = json_decode($datamain, true);
        $this->activedetails[key($maindata)] = $active;
        if($step == 1) {
            $this->reset('steps','activesteps','laststep','amount','mb');
        }
        if($step == 1 || $step == 2) {
            // $this->reset('laststep');
            $this->steps['type'] = $this->data['type'];
        }
        $this->activesteps[$index] = $active;
        if($index == 'last') {
            $this->laststep = true;
        } else {            
            $this->steps[$index] = $this->data[$index];
        }
    }

    public function submit()
    {
        dump($this->activedetails, $this->steps, $this->mb);
    }
}
