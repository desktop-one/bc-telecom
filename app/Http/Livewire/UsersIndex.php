<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class UsersIndex extends Component
{
    use WithPagination;

    public $searchTerm;
    public $role;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $this->role = str_replace('-', ' ', $this->role);
        // dump($this->role);
        $searchTerm = '%'.$this->searchTerm.'%';
        $users = User::where(function ($query) use($searchTerm) {
            $query->where('name', 'like', $searchTerm);
            $query->orWhere('email', 'like', $searchTerm);
        })
        ->whereHas(
            'roles', function($q){
                $q->where('name', $this->role);
            }
        )
        ->orderBy('id', 'desc')
        ->with(['permissions', 'roles', 'providers'])
        ->paginate();

        return view('livewire.users-index', compact('users'));
    }
}
