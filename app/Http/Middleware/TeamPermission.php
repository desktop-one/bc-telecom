<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TeamPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $flag)
    {
        if(!team_permission($flag)) {
            return redirect()->route('frontend.users.dashboard', app()->getLocale())->withErrors('You have no permission to access this module.');
        }
        return $next($request);
    }
}
