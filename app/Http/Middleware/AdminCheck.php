<?php

namespace App\Http\Middleware;

use Closure;

class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRoles = auth()->user()->roles->pluck('name')->toArray();   
        if(in_array('user', $userRoles)) {
            return redirect('/');
        }

        return $next($request);
    }
}
