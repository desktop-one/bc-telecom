<?php

namespace App\Http\Middleware;

use Closure;

class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {       
        // if(auth()->user()->is_confirm == 0 && auth()->user()->hasRole('company')) {
        //     \Session::flash('error', 'Need admin confirmation.');

        //     return redirect('/');
        // } 

        if(!auth()->user()->hasRole($role)) {
            return redirect('/')->withErrors('Only for '.ucfirst($role));
        }
        return $next($request);
    }
}
