<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('admin_sidebar', function ($menu) {
            // Dashboard
            $menu->add('<i class="cil-speedometer nav-icon"></i> Dashboard', [
                'route' => 'backend.dashboard',
                'class' => 'nav-item',
            ])
            ->data([
                'order'         => 1,
                'activematches' => 'admin/dashboard*',
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);


            // Separator: Access Management
            $menu->add('Management', [
                'class' => 'nav-title',
            ])
            ->data([
                'order'         => 101,
                'permission'    => ['view_users'],
            ]);

            // Settings
            $menu->add('<i class="nav-icon fas fa-cogs"></i> Settings', [
                'route' => 'backend.settings',
                'class' => 'nav-item',
            ])
            ->data([
                'order'         => 102,
                'activematches' => 'admin/settings*',
                'permission'    => ['view_settings'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

           

            // Access Control Dropdown
            $accessControl = $menu->add('<i class="nav-icon cil-shield-alt"></i> Access Control', [
                'class' => 'nav-group',
            ])
            ->data([
                'order'         => 103,
                'activematches' => [
                    'admin/users*',
                    'admin/roles*',
                ],
                'permission'    => ['view_users'],
            ]);
            $accessControl->link->attr([
                'class' => 'nav-link nav-group-toggle',
                'href'  => '#',
            ]);
             // Submenu: Roles
            $accessControl->add('<i class="nav-icon cil-people"></i> Roles', [
                'route' => 'backend.roles.index',
                'class' => 'nav-item d-none',
            ])
            ->data([
                'order'         => 103,
                'activematches' => 'admin/roles*',
                'permission'    => ['view_roles'],
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

            // Submenu: reseller
            $accessControl->add('<i class="nav-icon cil-people"></i> Reseller', [
                'url' => 'admin/users?role=reseller',
                'class' => 'nav-item',
            ])
            ->data([
                'order'         => 104,
                'activematches' => 'admin/users?role=reseller',
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);

            // Submenu: Admin
            $accessControl->add('<i class="nav-icon cil-people"></i> Admin', [
                'url' => 'admin/users?role=super-admin',
                'class' => 'nav-item',
            ])
            ->data([
                'order'         => 108,
                'activematches' => 'admin/users?role=super-admin',
            ])
            ->link->attr([
                'class' => 'nav-link',
            ]);


            // Access Permission Check
            $menu->filter(function ($item) {
                if ($item->data('permission')) {
                    if (auth()->check()) {
                        if (auth()->user()->hasRole('super admin')) {
                            return true;
                        } elseif (auth()->user()->hasAnyPermission($item->data('permission'))) {
                            return true;
                        }
                    }

                    return false;
                } else {
                    return true;
                }
            });

            // Set Active Menu
            $menu->filter(function ($item) {
                if ($item->activematches) {
                    $matches = is_array($item->activematches) ? $item->activematches : [$item->activematches];

                    foreach ($matches as $pattern) {
                        // dump(\Request::getRequestUri(), $pattern, request()->getHost());
                        // $current = 
                        if (strpos(\Request::getRequestUri(), $pattern) !== false) {
                            $item->activate();
                            $item->active();
                            if ($item->hasParent()) {
                                $item->parent()->activate();
                                $item->parent()->active();
                            }
                            // dd($pattern);
                        }
                    }
                }

                return true;
            });
        })->sortBy('order');

        return $next($request);
    }
}
