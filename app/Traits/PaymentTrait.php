<?php

namespace App\Traits;

use App\Models\ChatRoom;
use App\Models\Course;
use App\Models\CourseApply;
use App\Models\Message;
use App\Models\Payment;
use App\Models\User;


trait PaymentTrait {
    
    public function getCredit($slugData = [], $data = []) 
    {
        $slug = \Crypt::decryptString($slugData['slug']);
        $amount = \Str::of($slug)->explode('-')->first(); 

        $user = User::find(user_or_team());

        if(!empty($data)) {
            $this->store_payment([
                'type' => 'credit',
                'payment_id' => $data['id'],
                'payer_email' => $slugData['email'],
                'amount' => $data['amount']/100,
                'currency' => config('services.currency'),
                'payment_status' => $data['status'],
            ]);

            $user->deposit($amount, ['type' => 'deposit account credit', 'employe_type' => '']);
        }

        return ['price' => $amount];


    }

    protected function store_payment($arr_data = [], $apply = null)
    {
        $isPaymentExist = Payment::where('payment_id', $arr_data['payment_id'])->first();  
  
        if(!$isPaymentExist) {
            $payment = new Payment;
            $payment->type = $arr_data['type'];
            $payment->payment_id = $arr_data['payment_id'];
            $payment->payer_email = $arr_data['payer_email'];
            $payment->amount = $arr_data['amount'];
            $payment->currency = config('services.currency');
            $payment->payment_status = $arr_data['payment_status'];
            $payment->save();

            /* must comment after implement webhook */
            // $apply->payment_id = $arr_data['payment_id'];
            // $apply->payment_status = 1;
            // $apply->save();
            /* must comment after implement webhook */


        }
    }
}