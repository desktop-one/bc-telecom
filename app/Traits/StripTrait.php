<?php

namespace App\Traits;
use DB;

trait StripTrait {

    public function jsonSerialize()
    {
        return $this->__toArray(true);
    }
  
    public function stripCharge($price, $token, $description='')
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\Charge::create([
                "amount" => $price * 100,
                "currency" => config('services.currency'),
                "source" => $token,
                "description" => $description ?? "Making payment." 
        ]);
        return $response;
    }

    public function stripRefund($paymentId, $amount)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\Refund::create([
            "charge" => $paymentId, 
            "amount" => $amount,
        ]);
        return $response;
    }

    public function stripCancelSubscription($subscriptionId)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = $this->stripRetrieveSubscription($subscriptionId);
        $response->cancel();
        return $response;
    }

    public function stripDeleteSubscriptionItem($itemid)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\SubscriptionItem::retrieve($itemid);
        return $response->delete();
    }

    public function stripRetrieveSubscription($subscriptionId)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\Subscription::retrieve($subscriptionId);
        return $response;
    }

    public function stripGetPaymentMethod($paymentMethodId)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\PaymentMethod::retrieve($paymentMethodId);
        return $response;
    }

    public function stripGetPaymentMethods($paymentMethodId)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $response = \Stripe\PaymentMethod::retrieve($paymentMethodId);
        return $response;
    }

    public function stripSetupIntent($id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $session = \Stripe\SetupIntent::retrieve($id);
        return $session;
    }

    public function stripUpdateSubscriptionPayment($subid, $paymentid)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $session = \Stripe\Subscription::update(
            $subid,
            [
                'default_payment_method' => $paymentid,
            ]
        );
        return $session;
    }

    public function stripRetrieveSession($id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $session = \Stripe\Checkout\Session::retrieve(
            $id
        );
        return $session;
    }

    public function stripChangePaymentMethod($customerid, $subid, $successurl='', $cancelurl='')
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          'mode' => 'setup',
          'customer' => $customerid,
          'setup_intent_data' => [
            'metadata' => [
              'customer_id' => $customerid,
              'subscription_id' => $subid,
            ],
          ],
          'success_url' => route('frontend.users.subscriptions.index', app()->getLocale()).'?session_id={CHECKOUT_SESSION_ID}',
          'cancel_url' => route('frontend.users.subscriptions.index', app()->getLocale()).'?status=error',
        ]);
        return $session;
    }

    public function stripUpdateSubscription($subid, $type = 'month', $price=30, $currency='usd', $plan)
    {
        $subscription = $this->stripRetrieveSubscription($subid);
        // \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $priceId = $this->createPrice($price, $currency, $type);

        $response = \Stripe\Subscription::update(
            $subid, 
            [
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'price' => $priceId['id']
                    ],
                ],
                'metadata' => [
                    'plan_id' => $plan->id,
                    'plan_name' => $plan->title,
                ]
            ]
        );
        return $response;
    }

    public function stripSubscription($email, $token, $type = 'month', $price=30, $currency='usd')
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $customerId = $this->stripCustomer($email, $token);
        $priceId = $this->createPrice($price, $currency, $type);

        $response = \Stripe\Subscription::create([
            "customer" => $customerId, 
            'items' => [
                ['price' => $priceId['id']],
            ],
            // 'payment_settings' => [
            //     ['save_default_payment_method' => 'on_subscription']
            // ]
        ]);
        return $response;
    }

    public function stripRetriveCustomer($cusid)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $result = \Stripe\Customer::allSources($cusid);
        return $result;
    }

    public function stripCustomer($email, $token)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
       
        $result = DB::table('strip_customers')->where([
            'email' => $email
        ])->first();

        if($result) {
            return $result->strip_id;
        }

        $result = \Stripe\Customer::create([
            "email" => $email,
            "source" => $token
        ]);
        DB::table('strip_customers')->insert([
            'strip_id' => $result['id'],
            'email' => $email
        ]);
        return $result->id; 

        // }
    }

    public function createPrice(int $priceData, $currency='usd', $type='month')
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $result = DB::table('strip_prices')->where([
            'price' => $priceData,
            'currency' => $currency,
            'recurring' => $type,
        ])->first();

        if($result) {
            return [
                'id' => $result->strip_id
            ];
        }

        $product = $this->createProduct();
        $response = \Stripe\Price::create([
            'unit_amount' => $priceData * 100,
            'currency' => $currency,
            'recurring' => ['interval' => $type],
            'product' => $product['id'],
        ]);

        $data = $response->jsonSerialize();

        DB::table('strip_prices')->insert([
            'strip_id' => $data['id'],
            'price' => $priceData,
            'currency' => $currency,
            'recurring' => $type,
        ]);
        return $data;
    }

    public function createProduct($name='subscription')
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $response = \Stripe\Product::create([
            'name' => $name
        ]);

        $data = $response->jsonSerialize();
        return $data;

    }
}