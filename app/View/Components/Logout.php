<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Logout extends Component
{

    public $class;
    public $text;
    public $icon;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($class='',$text='Logout',$icon='')
    {
        $this->class = $class;
        $this->text = $text;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.logout');
    }
}
