<?php

namespace App\View\Components\buttons;

use Illuminate\View\Component;

class delete extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $class;
    public $id;
    public $route;

    public function __construct($class, $id, $route)
    {
        $this->class = $class;
        $this->id = $id;
        $this->route = $route;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.buttons.delete');
    }
}
