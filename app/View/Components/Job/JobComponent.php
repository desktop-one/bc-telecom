<?php

namespace App\View\Components\Job;

use Illuminate\View\Component;

class JobComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    
    public $job;
    public $type;
    public $list;

    public function __construct($job, $type='companys', $list="true")
    {
        $this->job = $job;
        $this->type = $type;
        $this->list = $list;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.job.job-component');
    }
}
