<?php

namespace App\View\Components\job;

use Illuminate\View\Component;

class job-details-component extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $job;

    public function __construct($job)
    {
        $this->job = $job;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.job.job-details-component');
    }
}
