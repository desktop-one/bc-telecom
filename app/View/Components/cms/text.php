<?php

namespace App\View\Components\cms;

use Illuminate\View\Component;

class text extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    public $value;
    public $title;
    public $required;

    public function __construct($name, $title, $value, $required="")
    {
        $this->name = $name;
        $this->value = $value;
        $this->title = $title;
        $this->required = $required;
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.cms.text');
    }
}
