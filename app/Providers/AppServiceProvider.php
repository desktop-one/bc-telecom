<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.backend-breadcrumbs', 'backendBreadcrumbs');

        Blade::directive('gTrans', function ($expression) {
            return "<?php echo gTrans($expression); ?>";
        });
        Blade::directive('date', function ($expression) {
            return "<?php echo date('d-m-Y', strtotime($expression)); ?>";
        });
        Blade::directive('datetime', function ($expression) {
            return "<?php echo date('d-m-Y H:i', strtotime($expression)); ?>";
        });
        Blade::directive('statusclass', function ($expression) {
            return "<?php echo is_null($expression) ? 'success' : 'danger'; ?>";
        });
        Blade::directive('statustext', function ($expression) {
            return "<?php echo is_null($expression) ? 'Delete' : 'Restore'; ?>";
        });

        if($this->app->environment('production')) {
            \URL::forceScheme('https');
        }

        /** video duration validation  'video:25' */
        Validator::extend('video_length', function($attribute, $value, $parameters, $validator) {

            // validate the file extension
            if(!empty($value->getClientOriginalExtension()) && ($value->getClientOriginalExtension() == 'mp4')){

                $ffprobe = \FFMpeg\FFProbe::create([
                    // 'ffmpeg.binaries'  => 'app/Libraries/ffmpeg/bin/ffmpeg.exe',
                    // 'ffprobe.binaries' => 'app/Libraries/ffmpeg/bin/ffprobe.exe' 
                    'ffmpeg.binaries'  => '/usr/local/bin/ffmpeg',
                    'ffprobe.binaries' => '/usr/local/bin/ffprobe'
                ]);
                $duration = $ffprobe
                    ->format($value->getRealPath()) // extracts file information
                    ->get('duration');
                return(round($duration) > $parameters[0]) ?false:true;
            } else{
                return false;
            }
        });
    }
}
