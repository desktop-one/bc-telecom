<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\BranchData\Entities\BranchData;
use Modules\Branch\Entities\Branch;

class BranchDataImport implements ToModel, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row['betshop']) {            
            $branch = Branch::where('title', 'like', '%' . $row['betshop'] . '%')->firstOrCreate(
                ['title' => $row['betshop']]
            );
            $date = intval($row['created_at']);
            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
            return new BranchData([
                'branch_id'  => $branch->id,
                'ticket_id'  => $row['ticket_id'],
                'created'    => $date,
                'payment'    => $row['payment'],
                'winning'    => $row['winnings'],
                'payout_tax' => $row['payout_tax'],
                'payout'     => $row['payout'],
                'status'     => $row['status'],
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
