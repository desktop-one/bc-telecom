<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Branch\Entities\Branch;
use Modules\LocationData\Entities\LocationData;

class LocationDataImport implements ToModel, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row['betshop']) {
            $branch = Branch::where('title', 'like', '%' . $row['betshop'] . '%')->firstOrCreate(
                ['title' => $row['betshop']]
            );
            $date = intval($row['date']);
            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
            return new LocationData([
                'branch_id'              => $branch->id,
                'date'                   => $date,
                'no_of_tickets'          => $row['no_of_tickets'],
                'payment'                => $row['payment'],
                'payment_tax'            => $row['payment_tax'],
                'payout'                 => $row['payout'],
                'payout_tax'             => $row['payout_tax'],
                'winnings'               => $row['winnings'],
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
