<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Description extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'lang',
        'text',
        'descriptionable_id',
        'descriptionable_type',
        'level',
    ];

    public function descriptionable()
    {
        return $this->morphTo();
    }
}
