<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Multimedia extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'content',
        'multimediable_id',
        'multimediable_type',
        'label',
    ];

    public function multimediable()
    {
        return $this->morphTo();
    }
}
