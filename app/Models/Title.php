<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Title extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'lang',
        'text',
        'titleable_id',
        'titleable_type',
        'level',
    ];

    public function titleable()
    {
        return $this->morphTo();
    }
}
