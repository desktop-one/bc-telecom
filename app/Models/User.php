<?php

namespace App\Models;

use App\Models\Presenters\UserPresenter;
use App\Models\Traits\HasHashedMediaTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasFactory;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;
    use HasHashedMediaTrait;
    use UserPresenter;

    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method',
        'password_confirmation',
    ];

    protected $dates = [
        'deleted_at',
        'date_of_birth',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return $this->name.' '.$this->last_name;
    }

    public function getSlugAttribute()
    {
        return $this->name.'-'.$this->last_name.'-'.$this->id;
    }

    public function getAvatarAttribute()
    {
        $file = $this->multimedia()->whereLabel('avatar')->orderByDesc('id')->first()->content ?? null;
        // return $file;
        if(!$file) {
            return asset('img/user.png');
        } else {
            if(fileExist($file, true)) {
                return asset($file);
            } else {
                return asset('img/user.png');
            }
        }
    }

    public function getProfileAttribute()
    {
        return $this->descriptions()->whereLevel('profile')->orderByDesc('id')->first()->text ?? null;
    }

    public function multimedia()
    {
        return $this->morphMany(Multimedia::class, 'multimediable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providers()
    {
        return $this->hasMany('App\Models\UserProvider');
    }

    public function userrequest()
    {
        return $this->hasMany(\Modules\UserRequest\Entities\UserRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Userprofile');
    }

    public function descriptions()
    {
        return $this->morphMany(Description::class, 'descriptionable');
    }
    
    /**
     * Get the list of users related to the current User.
     *
     * @return [array] roels
     */
    public function getRolesListAttribute()
    {
        return array_map('intval', $this->roles->pluck('id')->toArray());
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_NOTIFICATION_WEBHOOK');
    }
}
