<?php
use App\Http\Controllers\StripeWebhookController;
use App\Models\Course;
use App\Models\CourseBatch;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Modules\SitePackage\Entities\SitePackage;


Route::group(['namespace' => 'Payment'], function () { //'middleware' => ['auth', 'verified']

	Route::get('/strip-payment', 'StripPaymentController@index')->name('strip-payment');
	Route::post('/strip-charge', 'StripPaymentController@charge')->name('strip-charge');

	Route::get('/subscribe-update/{planid}/{lastsubid}', 'StripUserSubscribeController@updateSubscription')->name('subscribe-update');

	Route::get('/strip-user-subscribe-payment/{id}', 'StripUserSubscribeController@index')->name('strip-user-subscribe-payment');
	Route::post('/strip-user-subscribe-charge/{id}', 'StripUserSubscribeController@charge')->name('strip-user-subscribe-charge');
	Route::post('/strip-user-subscribe-confirm', 'StripUserSubscribeController@confirm')->name('strip-user-subscribe-confirm');

});

Route::post('webhooks/stripe', 'StripeWebhookController@handleWebhook');