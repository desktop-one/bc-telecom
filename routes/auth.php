<?php

use Illuminate\Support\Facades\Route;


// Authentication Routes...
Route::get('login/{role?}', 'LoginController@showLoginForm')->name('login');
Route::post('confirm-login', 'LoginController@confirmLogin')->name('confirm-login');
Route::post('login/{role?}', 'LoginController@login');
Route::post('logout', 'LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register/{role?}', 'RegisterController@showRegistrationForm')->name('register');
Route::post('register/{role?}', 'RegisterController@register');


// Password Reset Routes...
Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

// Password Confirmation Routes...
Route::get('password/confirm', 'ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'ConfirmPasswordController@confirm');

// Email Verification Routes...
Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'VerificationController@resend')->name('verification.resend');

//social login
Route::get('social-login/{type}', 'Social\LoginController@redirectToProvider');
Route::get('social-login/{type}/callback', 'Social\LoginController@handleProviderCallback');

Route::get('subscription', 'SubscriptionController@index')->name('subscriptions.index');
Route::post('subscription-create', 'SubscriptionController@create')->name('subscriptions.create');
Route::post('subscription-store', 'SubscriptionController@store')->name('subscriptions.store');
