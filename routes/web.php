<?php
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

include __DIR__.'/other.php';
include __DIR__.'/payment.php';
    
// frontend
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {

    Route::get('/', 'FrontendController@index')->name('index');     
    Route::get('/page/{pagename}', 'FrontendController@page')->name('page'); 

});

// Route::get('cronjob/wC5EFJ-8TLdon-96Y7mo-Wes5Gw-4UwKJp', [\App\Http\Controllers\CronJobController::class, 'checkTeacherCompleteCourse']);