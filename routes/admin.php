<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


// backend
Route::group(['namespace' => 'Backend', 'prefix' => '/admin', 'as' => 'backend.', 'middleware' => ['auth', 'admincheck']], function () {

    Route::resources([

        'chats' => ChatController::class,
    ]);

    /**
     * Backend Dashboard
     * Namespaces indicate folder structure.
     */
    Route::get('/', 'BackendController@index')->name('home');
    Route::get('dashboard', 'BackendController@index')->name('dashboard');

    // Settings Routes
    // 
    Route::group(['middleware' => ['permission:edit_settings']], function () {
        $module_name = 'settings';
        $controller_name = 'SettingController';
        Route::get("$module_name", "$controller_name@index")->name("$module_name");
        Route::post("$module_name", "$controller_name@store")->name("$module_name.store");
    });

    /*
    *
    *  Notification Routes
    *
    * ---------------------------------------------------------------------
    */
    $module_name = 'notifications';
    $controller_name = 'NotificationsController';
    Route::get("$module_name", ['as' => "$module_name.index", 'uses' => "$controller_name@index"]);
    Route::get("$module_name/markAllAsRead", ['as' => "$module_name.markAllAsRead", 'uses' => "$controller_name@markAllAsRead"]);
    Route::delete("$module_name/deleteAll", ['as' => "$module_name.deleteAll", 'uses' => "$controller_name@deleteAll"]);
    Route::get("$module_name/{id}", ['as' => "$module_name.show", 'uses' => "$controller_name@show"]);

    // Roles Routes
    // 
    $module_name = 'roles';
    $controller_name = 'RolesController';
    Route::resource("$module_name", "$controller_name");

    
    // Users Routes
    // 
    $module_name = 'users';
    $controller_name = 'UserController';
    Route::get("$module_name/profile/{id}", ['as' => "$module_name.profile", 'uses' => "$controller_name@profile"]);
    Route::get("$module_name/profile/{id}/edit", ['as' => "$module_name.profileEdit", 'uses' => "$controller_name@profileEdit"]);
    Route::patch("$module_name/profile/{id}/edit", ['as' => "$module_name.profileUpdate", 'uses' => "$controller_name@profileUpdate"]);
    Route::get("$module_name/emailConfirmationResend/{id}", ['as' => "$module_name.emailConfirmationResend", 'uses' => "$controller_name@emailConfirmationResend"]);
    Route::delete("$module_name/userProviderDestroy", ['as' => "$module_name.userProviderDestroy", 'uses' => "$controller_name@userProviderDestroy"]);
    Route::get("$module_name/profile/changeProfilePassword/{id}", ['as' => "$module_name.changeProfilePassword", 'uses' => "$controller_name@changeProfilePassword"]);
    Route::patch("$module_name/profile/changeProfilePassword/{id}", ['as' => "$module_name.changeProfilePasswordUpdate", 'uses' => "$controller_name@changeProfilePasswordUpdate"]);
    Route::get("$module_name/changePassword/{id}", ['as' => "$module_name.changePassword", 'uses' => "$controller_name@changePassword"]);
    Route::patch("$module_name/changePassword/{id}", ['as' => "$module_name.changePasswordUpdate", 'uses' => "$controller_name@changePasswordUpdate"]);
    Route::get("$module_name/trashed", ['as' => "$module_name.trashed", 'uses' => "$controller_name@trashed"]);
    Route::patch("$module_name/trashed/{id}", ['as' => "$module_name.restore", 'uses' => "$controller_name@restore"]);
    Route::get("$module_name/index_data", ['as' => "$module_name.index_data", 'uses' => "$controller_name@index_data"]);
    Route::get("$module_name/index_list", ['as' => "$module_name.index_list", 'uses' => "$controller_name@index_list"]);

    Route::get("$module_name/user-details/{user}", ['as' => "$module_name.details", 'uses' => "$controller_name@details"]);
    Route::get("$module_name/propertys/{user}", ['as' => "$module_name.propertys", 'uses' => "$controller_name@propertys"]);
    Route::get("$module_name/services/{user}", ['as' => "$module_name.services", 'uses' => "$controller_name@services"]);
    Route::get("$module_name/renew-services/{userservice}", ['as' => "$module_name.renewservices", 'uses' => "$controller_name@renewservices"]);
    Route::resource("$module_name", "$controller_name");

    Route::get("$module_name/{id}/varification", ['as' => "$module_name.varification", 'uses' => "$controller_name@varification"]);
    Route::patch("$module_name/{id}/accessQuestion", ['as' => "$module_name.accessQuestion", 'uses' => "$controller_name@accessQuestion"]);
    Route::get("$module_name/{id}/student-report", ['as' => "$module_name.studentReport", 'uses' => "$controller_name@studentReport"]);
    Route::patch("$module_name/{id}/confirmed", ['as' => "$module_name.confirmed", 'uses' => "$controller_name@confirmed", 'middleware' => ['permission:block_users']]);
    Route::patch("$module_name/{id}/block", ['as' => "$module_name.block", 'uses' => "$controller_name@block", 'middleware' => ['permission:block_users']]);
    Route::patch("$module_name/{id}/unblock", ['as' => "$module_name.unblock", 'uses' => "$controller_name@unblock", 'middleware' => ['permission:block_users']]);
});