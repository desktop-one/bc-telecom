<?php


/* Generate storage link */
Route::get('link', function () {
    system("rm -rf ".escapeshellarg(public_path('storage')));
    // \File::deleteDirectory(public_path('storage'));
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    return back();
});

/* clear all cash */
Route::get('clear', function () {
    \Illuminate\Support\Facades\Artisan::call('optimize:clear');
    return back();
});

/* remove session */
Route::get('s-clear', function() {
    Session::flush();
    return back();
});

// Language Switch
Route::get('language/{language}', 'LanguageController@switch')->name('language.switch');
Route::get('home-language/{language}', 'LanguageController@home')->name('language.home');


// setting 
// $user_registration = user_registration();
// Auth::routes(['verify' => true, 'register' => $user_registration]);