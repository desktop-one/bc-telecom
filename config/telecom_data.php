<?php

return [
	'type' => [
		'mobile_banking' => [
			'name' => 'Mobile Banking',
			'img' => 'img/bkash.png',
			'action' => 'mobile_banking_operator',
			'step' => 1
		],
		'mobile_recharge' => [
			'name' => 'Mobile Recharge',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => 1
		],
		'mobile_mb' => [
			'name' => 'Mobile MB',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => 1
		],
		'package' => [
			'name' => 'Package',
			'img' => 'img/bkash.png',
			'action' => '',
			'step' => 1
		],
	],
	'mobile_banking_operator' => [
		'bkash' => [
			'name' => 'Bkash',
			'img' => 'img/bkash.png',
			'action' => 'mobile_banking_type',
			'step' => 2
		],
		'nagod' => [
			'name' => 'Nagod',
			'img' => 'img/bkash.png',
			'action' => 'mobile_banking_type',
			'step' => 2
		],
		'rocket' => [
			'name' => 'Rocket',
			'img' => 'img/bkash.png',
			'action' => 'mobile_banking_type',
			'step' => 2
		],
		'upay' => [
			'name' => 'Upay',
			'img' => 'img/bkash.png',
			'action' => 'mobile_banking_type',
			'step' => 2
		],
	],
	'mobile_banking_type' => [
		'send_money' => [
			'name' => 'Send Money',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'cash_out' => [
			'name' => 'Cash Out',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'electricity_bill' => [
			'name' => 'Electricity Bill',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'gas_bill' => [
			'name' => 'Gass Bill',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'watter_bill' => [
			'name' => 'Watter Bill',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'other' => [
			'name' => 'Other',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
	],
	'mobile_operator' => [
		'grameenphone' => [
			'name' => 'GrameenPhone',
			'img' => 'img/bkash.png',
			'action' => 'last',
			'step' => 2
		],
		'banglalink' => [
			'name' => 'Banglalink',
			'img' => 'img/bkash.png',
			'action' => 'last',
			'step' => 2
		],
		'airtel' => [
			'name' => 'Airtel',
			'img' => 'img/bkash.png',
			'action' => 'last',
			'step' => 2
		],
		'robi' => [
			'name' => 'Robi',
			'img' => 'img/bkash.png',
			'action' => 'last',
			'step' => 2
		],
		'teletalk' => [
			'name' => 'Teletalk',
			'img' => 'img/bkash.png',
			'action' => 'last',
			'step' => 2
		],
	],
	'user_type' => [
		'personal' => [
			'name' => 'Personal',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'agent' => [
			'name' => 'Agent',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
	],
	'mobile_recharge_type' => [
		'recharge' => [
			'name' => 'Recharge',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'internet' => [
			'name' => 'Internet (MB)',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
		'minute' => [
			'name' => 'Minutes',
			'img' => 'img/bkash.png',
			'action' => 'mobile_operator',
			'step' => ''
		],
	]
];