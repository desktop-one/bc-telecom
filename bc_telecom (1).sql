-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 04, 2022 at 04:22 PM
-- Server version: 5.7.36
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bc_telecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `batch_uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `causer` (`causer_type`,`causer_id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branchs`
--

DROP TABLE IF EXISTS `branchs`;
CREATE TABLE IF NOT EXISTS `branchs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `testing` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `descriptions`
--

DROP TABLE IF EXISTS `descriptions`;
CREATE TABLE IF NOT EXISTS `descriptions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `text` text COLLATE utf8_unicode_ci,
  `descriptionable_id` bigint(20) NOT NULL,
  `descriptionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'primary',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `generated_conversions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_uuid_unique` (`uuid`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`),
  KEY `media_order_column_index` (`order_column`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
CREATE TABLE IF NOT EXISTS `multimedia` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `multimediable_id` bigint(20) NOT NULL,
  `multimediable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view_backend', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(2, 'edit_settings', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(3, 'view_logs', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(4, 'view_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(5, 'add_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(6, 'edit_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(7, 'delete_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(8, 'restore_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(9, 'block_users', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(10, 'view_roles', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(11, 'add_roles', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(12, 'edit_roles', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(13, 'delete_roles', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(14, 'restore_roles', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(15, 'view_backups', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(16, 'add_backups', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(17, 'create_backups', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(18, 'download_backups', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(19, 'delete_backups', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(20, 'view_posts', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(21, 'add_posts', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(22, 'edit_posts', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(23, 'delete_posts', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(24, 'restore_posts', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(25, 'view_categories', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(26, 'add_categories', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(27, 'edit_categories', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(28, 'delete_categories', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(29, 'restore_categories', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(30, 'view_tags', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(31, 'add_tags', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(32, 'edit_tags', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(33, 'delete_tags', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(34, 'restore_tags', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(35, 'view_comments', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(36, 'add_branchs', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(37, 'edit_branchs', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(38, 'delete_branchs', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(39, 'restore_branchs', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(40, 'user_account_setting', 'web', NULL, NULL),
(41, 'user_manage_job_posted', 'web', NULL, NULL),
(42, 'user_manage_job_profile', 'web', NULL, NULL),
(43, 'user_job_applied', 'web', NULL, NULL),
(44, 'user_inbox', 'web', NULL, NULL),
(45, 'user_favourite', 'web', NULL, NULL),
(46, 'user_wallet', 'web', NULL, NULL),
(47, 'user_notification', 'web', NULL, NULL),
(48, 'user_manage_subscription', 'web', NULL, NULL),
(49, 'user_manage_communication', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01'),
(2, 'reseller', 'web', '2022-07-19 07:58:01', '2022-07-19 07:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8mb4_unicode_ci,
  `type` char(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'string',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `val`, `type`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'app_name', 'BC Telecom', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-11-30 17:10:24', NULL),
(2, 'footer_text', 'Empowered by CodeVillage', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-11-30 17:10:24', NULL),
(3, 'show_copyright', '1', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(4, 'email', 'info@example.com', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(5, 'facebook_url', '#', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(6, 'twitter_url', '#', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(7, 'instagram_url', '#', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(8, 'linkedin_url', '#', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(9, 'youtube_url', '#', 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(10, 'meta_site_name', 'BC Telecom', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-11-30 17:10:24', NULL),
(11, 'meta_description', 'BC Telecom', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-11-30 17:10:24', NULL),
(12, 'meta_keyword', 'BC Telecom', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-11-30 17:10:24', NULL),
(13, 'meta_image', 'img/default_banner.jpg', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(14, 'meta_fb_app_id', '569561286532601', 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(15, 'meta_twitter_site', NULL, 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(16, 'meta_twitter_creator', NULL, 'text', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(17, 'google_analytics', NULL, 'string', 1, 1, NULL, '2022-07-19 08:44:55', '2022-07-19 08:44:55', NULL),
(18, 'amounts', '', 'string', NULL, NULL, NULL, '2022-07-20 10:29:02', '2022-07-20 10:29:02', NULL),
(19, 'website_url', 'https://bc-telecom.com', 'string', NULL, NULL, NULL, '2022-07-20 10:29:02', '2022-11-30 17:10:24', NULL),
(20, 'phone', '12345678', 'string', NULL, NULL, NULL, '2022-07-20 10:29:02', '2022-07-20 10:29:02', NULL),
(21, 'address', 'BC Telecom', 'string', NULL, NULL, NULL, '2022-07-20 10:29:02', '2022-11-30 17:10:24', NULL),
(22, 'discord_url', '#', 'string', NULL, NULL, NULL, '2022-07-20 10:29:02', '2022-07-20 10:29:02', NULL),
(23, 'site_logo', '/storage/5b26238a-91c0-4aa4-9b09-8a0ae87bad82.png', 'file', NULL, NULL, NULL, '2022-07-20 10:29:10', '2022-11-30 17:09:25', NULL),
(24, 'site_favicon', '/storage/c41ae415-fd84-4809-996a-2814b317e597.png', 'file', NULL, NULL, NULL, '2022-07-20 10:29:24', '2022-11-30 17:09:25', NULL),
(25, 'site_logo_black', '/storage/1b1adb23-280d-4c11-b88b-407b7206b885.png', 'file', NULL, NULL, NULL, '2022-08-09 12:21:06', '2022-11-30 17:09:25', NULL),
(26, 'footer_text2', 'It is a long established fact that a reader will be distracted by the readable content of a page.', 'string', NULL, NULL, NULL, '2022-09-29 07:41:10', '2022-09-29 07:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

DROP TABLE IF EXISTS `titles`;
CREATE TABLE IF NOT EXISTS `titles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `text` longtext CHARACTER SET utf8,
  `titleable_id` bigint(20) NOT NULL,
  `titleable_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userprofiles`
--

DROP TABLE IF EXISTS `userprofiles`;
CREATE TABLE IF NOT EXISTS `userprofiles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_metadata` text COLLATE utf8mb4_unicode_ci,
  `last_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_count` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operator_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription_status` tinyint(1) NOT NULL DEFAULT '0',
  `last_subscription_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `get_free_subscription` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `sex` enum('male','female','others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_confirm` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `login_count` int(11) DEFAULT '0',
  `last_activity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_verified_status` enum('pending','accepted','rejected','blocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'accepted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `reseller_id`, `name`, `last_name`, `email`, `phone`, `operator_name`, `birth_date`, `email_verified_at`, `password`, `remember_token`, `otp`, `subscription_status`, `last_subscription_id`, `get_free_subscription`, `created_at`, `updated_at`, `deleted_at`, `sex`, `role`, `is_confirm`, `status`, `is_online`, `login_count`, `last_activity`, `admin_verified_status`) VALUES
(1, NULL, 'Super', NULL, 'admin@admin.com', '01673106669', NULL, NULL, NULL, '$2y$10$8LaJomE24BsvhgJ5qvmH7.l2MRy1OtRGB42m3WgkFsP6sSnP5FYOq', 'iXbRecL6cthAiodH7nHJxhs7UUqfbI6EdBA2gmqiqFsnM8tWxxJvzkhmZe8y', NULL, 0, NULL, 0, '2022-03-09 05:46:26', '2022-11-22 05:35:17', NULL, 'male', 'admin', 1, 1, 1, 0, '2022-11-22 11:36:17', 'accepted'),
(2, NULL, 'dpirubel+14@gmail.com', NULL, 'dpirubel+14@gmail.com', '01673106668', NULL, NULL, '2022-12-02 12:42:20', '$2y$10$YoHWfyXZwALLAPAsQ1o6.Ol59gw4WFzL3tnTxcfiMbZAu9lLGSQrC', NULL, NULL, 0, NULL, 1, '2022-12-02 12:42:20', '2022-12-02 12:42:20', NULL, NULL, 'reseller', 1, 1, 0, 0, NULL, 'accepted');

-- --------------------------------------------------------

--
-- Table structure for table `user_packages`
--

DROP TABLE IF EXISTS `user_packages`;
CREATE TABLE IF NOT EXISTS `user_packages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '0',
  `operator` varchar(50) DEFAULT '0',
  `m_mb` varchar(50) DEFAULT '0',
  `m_minute` varchar(50) DEFAULT '0',
  `m_bank_op` varchar(50) DEFAULT '0',
  `m_bank_type` varchar(50) DEFAULT '0',
  `m_bank_user` varchar(50) DEFAULT '0',
  `number` varchar(50) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_user_packages_users` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `user_phonebooks`
--

DROP TABLE IF EXISTS `user_phonebooks`;
CREATE TABLE IF NOT EXISTS `user_phonebooks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `number` varchar(50) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_user_phonebooks_users` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `user_phonebooks`
--

INSERT INTO `user_phonebooks` (`id`, `user_id`, `name`, `number`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'My Boss', '242342342', 1, '2022-12-04 09:51:51', '2022-12-04 09:51:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_providers`
--

DROP TABLE IF EXISTS `user_providers`;
CREATE TABLE IF NOT EXISTS `user_providers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_providers_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_requests`
--

DROP TABLE IF EXISTS `user_requests`;
CREATE TABLE IF NOT EXISTS `user_requests` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_package_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT '0',
  `operator` varchar(50) NOT NULL DEFAULT '0',
  `m_mb` varchar(50) DEFAULT '0',
  `m_minute` varchar(50) DEFAULT '0',
  `m_bank_op` varchar(50) DEFAULT '0',
  `m_bank_type` varchar(50) DEFAULT '0',
  `m_bank_type_other` varchar(50) DEFAULT '0',
  `m_bank_user` varchar(50) DEFAULT '0',
  `number` varchar(50) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_requests_users` (`user_id`),
  KEY `FK_user_requests_user_packages` (`user_package_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
